# libhdbpp-tofile

Is a fork of [libhdbpp-timescale](https://gitlab.com/tango-controls/hdbpp/libhdbpp-timescale), specifically for the purpose of logging DB insert to rotating log files.  This is meant to be used in conjunction with the ElasticStack or any other log shipping machinery to enable lightweight archiving consolidation.



# From the Fork

- [libhdbpp-tofile](#libhdbpp-tofile)
- [From the Fork](#from-the-fork)
  - [v0.9.0 To v0.10.0 Update](#v090-to-v0100-update)
  - [Cloning](#cloning)
  - [Bug Reports + Feature Requests](#bug-reports--feature-requests)
  - [Documentation](#documentation)
  - [Building](#building)
    - [Building Process](#building-process)
      - [Ubuntu](#ubuntu)
    - [Running tests and benchmark](#running-tests-and-benchmark)
  - [Installing](#installing)
    - [System Dependencies](#system-dependencies)
    - [Installation](#installation)
  - [License](#license)

HDB++ backend library for the TimescaleDb extenstion to Postgresql. This library is used by events subscribers to archive events from a Tango Controls system.

The library requires a correctly configured [TimescaleDb](https://www.timescale.com/) installation.

The library has been build against a number of other projects, these have been integrated into the repository as sub modules. This may change at a later date if the build system is improved. Current sub modules are:

* spdlog - Logging system
* Catch2 - Unit test subsystem

## v0.9.0 To v0.10.0 Update

This revision changes how both scalar and spectrum strings are stored. In 0.9.0 strings were escaped and quoted before being stored in the database. This had the effect that when the strings were retrieved they were still escaped/quoted. For consistency scalar strings were stored escaped/quoted.

To fix this, spectrum's of strings are now stored via insert strings using both the ARRAY syntax and dollar escape method. This means when they are retrieved frm the database they are longer escaped/quoted. To match this, scalar strings are also no longer stored escaped/quoted.

## Cloning

Currently this project is configured to express its dependencies as submodules. This may change in future if there is time to explore, for example, the Meson build system. To successfully clone the project and all its dependencies use the following:

```bash
git clone --recurse-submodules https://gitlab.com/tango-controls/hdbpp/libhdbpp-tofile.git
```

## Bug Reports + Feature Requests

Please file the bug reports and feature requests in the issue tracker

## Documentation

* See the [doc](doc/README.md) directory for documentation on various topics, including building and installing.
* General documentations on the Tango HDB system can be found [here](http://tango-controls.readthedocs.io/en/latest/administration/services/hdbpp/index.html#hdb-an-archiving-historian-service)

## Building

For a full reference on building please refer to the [documentation](doc/build.md). This is a small summary of the standard building steps.

### Building Process

To compile this library, first ensure it has been recursively cloned so all submodules are present in /thirdparty. The build system uses pkg-config to find some dependencies, for example Tango. If Tango is not installed to a standard location, set PKG_CONFIG_PATH, i.e.

```bash
export PKG_CONFIG_PATH=/non/standard/tango/install/location
```

Then to build just the library:

```bash
mkdir -p build
cd build
cmake ..
make
```

The pkg-config path can also be set with the cmake argument CMAKE_PREFIX_PATH. This can be set on the command line at configuration time, i.e.:

```bash
...
cmake -DCMAKE_PREFIX_PATH=/non/standard/tango/install/location ..
...
```

#### Ubuntu

When using Postgres from the Ubuntu repositoris, it appears to install its development libraries in a slightly different location. Some info on this issue [here](https://gitlab.kitware.com/cmake/cmake/issues/17223). In this case, we set the PostgreSQL_TYPE_INCLUDE_DIR variable directly when calling cmake:

```
cmake -DPostgreSQL_TYPE_INCLUDE_DIR=/usr/include/postgresql/ ..
```

This should replace the call to cmake in the previous section.

### Running tests and benchmark

Please refer to the full [documentation](doc/build.md).

## Installing

For a full reference on installation please refer to the [documentation](doc/install.md). This is a small summary of the standard installation steps.
All submodules are combined into the final library for ease of deployment. This means just the libhdbpp-tofile.so binary needs deploying to the target system.

### System Dependencies

The running system requires libpq5 installed to support the calls Postgresql. On Debian/Ubuntu this can be deployed as follows:

```bash
sudo apt-get install libpq5
```

### Installation

After the build has completed, simply run:

```
sudo make install
```

The shared library will be installed to /usr/local/lib on Debian/Ubuntu systems.

## License

The source code is released under the LGPL3 license and a copy of this license is provided with the code.
